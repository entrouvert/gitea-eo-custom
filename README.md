# gitea-eo-custom

Adaptations locales à Gitea, principalement l’ajout de scripts pour générer
des pages de rapport :

* lag.py (→ /lag.html): liste de commits pas encore repris dans des versions
  tagguées
* reviews.py (→ /reviews.html): liste des relectures demandées

Ces pages sont générées statiquement toutes les cinq minutes via cron, doivent
être servies par le serveur web, par exemple pour nginx :

```
location /lag.html { alias /var/www/html/lag.html; }
location /reviews.html { alias /var/www/html/reviews.html; }
```
