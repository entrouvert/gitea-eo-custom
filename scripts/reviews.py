#! /usr/bin/env python3

import collections
import configparser
import datetime
import psycopg2

config = configparser.ConfigParser()
config.read('/etc/gitea/eo-custom.ini')
db_connect_string = config.get('settings', 'db')

conn = psycopg2.connect(db_connect_string)
cursor = conn.cursor()
cursor.execute(
    """select "user".full_name, "user".name, repository.name, issue.index, review.created_unix
  from review, "user", issue, repository, pull_request
  where "user".id = reviewer_id
    and review.type = 4
    and review.issue_id = issue.id
    and issue.repo_id = repository.id
    and issue.index = pull_request.index
    and issue.is_closed = false
    and pull_request.issue_id = issue.id
    and pull_request.has_merged = false
    and review.id in (select max(id) from review
                       where issue_id = issue.id
                         and review.type in (1, 3, 4)
                         and reviewer_id = reviewer_id
                         and dismissed = false)
order by "user".full_name, repository.name, review.created_unix
"""
)

print(
    '''<!DOCTYPE html>
<html lang="fr-FR" class="theme-auto">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Relectures en attente - Gitea: Git with a cup of tea</title>

  <meta name="default-theme" content="auto">

  <link rel="icon" href="/assets/img/favicon.svg" type="image/svg+xml">
  <link rel="alternate icon" href="/assets/img/favicon.png" type="image/png">
  <link rel="stylesheet" href="/assets/css/index.css">
    <link rel="stylesheet" href="/assets/css/theme-auto.css?v=1.19.3">
    <link rel="stylesheet" href="/assets/custom.css">

<style>
body { font-family: sans-serif; }
h1 { font-weight: 300; font-size: 2.3rem;}
h2 { font-weight: 300; font-size: 1.6rem;}
a.anchor-link { font-size: 1rem; visibility: hidden;}
h2:hover a.anchor-link { visibility: visible; }
</style><body>
<div class="full height">
  <nav id="navbar" aria-label="Barre de navigation">
    <div class="navbar-left ui secondary menu">
    <a class="item" id="navbar-logo" href="/" aria-label="Tableau de bord">
       <img width="30" height="30" src="/assets/img/logo.svg" alt="Logo" aria-hidden="true">
    </a>
    <a class="item" href="/issues">Tickets</a>
    <a class="item" href="/pulls">Demandes d'ajout</a>
    <a class="item" href="/milestones">Jalons</a>
    <a class="item" href="/explore/repos">Explorateur</a>
    <a class="item active" href="/reviews.html">Rapports</a>
    <a class="item" href="https://dev.entrouvert.org">Redmine</a>
    <a class="item" href="https://jenkins.entrouvert.org">Jenkins</a>
    </div>
  </nav>
<div class="page-content explore">

<div class="ui secondary pointing tabular top attached borderless stackable menu new-menu navbar">
  <a class="active item" href="/reviews.html">Relectures demandées</a>
  <a class="item" href="/lag.html">Commits pas encore taggués</a>
</div>

<div class="ui container">
<h1>Relectures demandées à :</h1>
'''
)

authors = collections.defaultdict(list)

for fullname, username, repository, pr, created_unix in cursor.fetchall():
    authors[username].append((fullname, username, repository, pr, created_unix))

for author_key, reviews in sorted(authors.items()):
    fullname, username, repository, pr, created_unix = reviews[0]
    print(f'''<h2 id="{username}">{fullname} <a class="anchor-link" href="#{username}">¶</a></h2>''')
    print('<ul>')
    for review in reviews:
        fullname, username, repository, pr, created_unix = review
        print(
            f'''<li><a href="/entrouvert/{repository}/pulls/{pr}">{repository} #{pr}</a>
    <span class="datetime">({datetime.datetime.fromtimestamp(created_unix)})</span></li>
'''
        )
    print('</ul>')

print('''</div></div></div></html>''')
