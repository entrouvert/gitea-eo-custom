#! /usr/bin/env python3

import configparser
import datetime
import glob
import html
import os
import subprocess
import requests

recent = datetime.datetime.now() - datetime.timedelta(days=28)
three_days_ago = datetime.datetime.now() - datetime.timedelta(days=3)

config = configparser.ConfigParser()
config.read('/etc/gitea/eo-custom.ini')
access_token = config.get('settings', 'access_token')

archived = set()

try:
    for i in range(50):
        repositories = requests.get(
            'https://git.entrouvert.org/api/v1/orgs/entrouvert/repos?limit=50&page=%s&token=%s'
            % (i + 1, access_token),
            headers={'accept': 'application/json'},
        ).json()
        if not repositories:
            break
        archived |= set(x['name'] for x in repositories if x.get('archived'))
except requests.exceptions.RequestException:
    pass

print(
    '''<!DOCTYPE html>
<html lang="fr-FR" class="theme-auto">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Commits pas encore taggués - Gitea: Git with a cup of tea</title>

  <meta name="default-theme" content="auto">

  <link rel="icon" href="/assets/img/favicon.svg" type="image/svg+xml">
  <link rel="alternate icon" href="/assets/img/favicon.png" type="image/png">
  <link rel="stylesheet" href="/assets/css/index.css">
    <link rel="stylesheet" href="/assets/css/theme-auto.css?v=1.19.3">
    <link rel="stylesheet" href="/assets/custom.css">

<style>
body { font-family: sans-serif; }
h1 { font-weight: 300; font-size: 2.3rem;}
h2 { font-weight: 300; font-size: 1.6rem;}
a.anchor-link { font-size: 1rem; visibility: hidden;}
h2:hover a.anchor-link { visibility: visible; }

table { border-collapse: collapse; }
td.repos { width: 20em; padding-top: 0.5em;}
.lag { color: red; }
.log { display: none; }
pre { margin: 0; font-size: 90%; color: #444; background: #fafafa; }

</style><body>
<div class="full height">
  <nav id="navbar" aria-label="Barre de navigation">
    <div class="navbar-left ui secondary menu">
    <a class="item" id="navbar-logo" href="/" aria-label="Tableau de bord">
       <img width="30" height="30" src="/assets/img/logo.svg" alt="Logo" aria-hidden="true">
    </a>
    <a class="item" href="/issues">Tickets</a>
    <a class="item" href="/pulls">Demandes d'ajout</a>
    <a class="item" href="/milestones">Jalons</a>
    <a class="item" href="/explore/repos">Explorateur</a>
    <a class="item active" href="/reviews.html">Rapports</a>
    <a class="item" href="https://dev.entrouvert.org">Redmine</a>
    <a class="item" href="https://jenkins.entrouvert.org">Jenkins</a>
    </div>
  </nav>
<div class="page-content explore">

<div class="ui secondary pointing tabular top attached borderless stackable menu new-menu navbar">
  <a class="item" href="/reviews.html">Relectures demandées</a>
  <a class="active item" href="/lag.html">Commits pas encore taggués</a>
</div>

<div class="ui container">
<h1>Commits pas encore taggués</h1>
'''
)


print(
    '''
<p><label><input id="display-commits" type="checkbox"> Afficher les commits</label></p>
<table>'''
)

for repos in sorted(glob.glob('/var/lib/gitea/data/gitea-repositories/entrouvert/*.git')):
    repos_name = os.path.basename(repos).removesuffix('.git')
    if repos_name in archived:
        continue
    if repos_name.startswith('misc-'):  # personal repositories
        continue
    if repos_name in (  # ignore list:
        # déployés/exploités depuis le dépôt eobuilder
        'barbacompta',
        'debian-django-ckeditor',
        'scrutiny',
        'git-redmine',
        'gitea-deb',
        'gitea-eo-custom',
        'jenkins-lib',
        # test
        'bidon',
        # installé depuis les sources
        'publik-devinst',
        # iMio
        'imio-publik-themes',
        'combo-plugin-imio-townstreet',
        'passerelle-imio-ts1-datasources',
        'passerelle-imio-extra-fees',
        'passerelle-imio-ia-delib',
        'passerelle-imio-liege-lisrue',
        'passerelle-imio-liege-rn',
        'passerelle-imio-tax-compute',
        'imio-teleservices-templatetags',
        'authentic2-wallonie-connect',
    ):
        continue
    os.chdir(repos)
    try:
        latest = datetime.datetime.strptime(
            subprocess.check_output(
                ['git', 'log', '-1', '--pretty=format:%ci'], stderr=subprocess.PIPE
            ).decode('ascii')[:18],
            '%Y-%m-%d %H:%M:%S',
        )
    except subprocess.CalledProcessError:
        continue
    if latest < recent:
        continue
    try:
        described = (
            subprocess.check_output(['git', 'describe'], stderr=subprocess.PIPE).decode('ascii').strip()
        )
    except subprocess.CalledProcessError:
        continue
    classname = ''
    if not '-g' in described:
        continue
    if latest < three_days_ago:
        classname = 'lag'
    module_name = repos.split('/')[-1].removesuffix('.git')
    print(
        '<tr class="repos %s"><td class="repos"><a href="https://git.entrouvert.org/entrouvert/%s/graph?branch=refs/heads/main">%s</td> <td class="described"><tt>%s</tt></td><td></td></tr>'
        % (classname, module_name, module_name, described)
    )
    count = int(described.split('-')[1])
    try:
        log = (
            subprocess.check_output(['git', 'log', '--oneline', 'HEAD~%s..' % count], stderr=subprocess.PIPE)
            .decode()
            .strip()
        )
    except subprocess.CalledProcessError:
        pass
    else:
        print('<tr class="log"><td colspan="3"><pre>%s</pre></td></tr>' % html.escape(log))

print(
    '''</table></div></div></body>
<script>
const checkbox = document.getElementById('display-commits');
checkbox.addEventListener('change', function(e) {
  document.querySelectorAll('tr.log').forEach(function(tr) {
    if (checkbox.checked) {
      tr.style.display = 'table-row';
    } else {
      tr.style.display = 'none';
    }
  });
});
const event = new Event('change');
checkbox.dispatchEvent(event);
</script>
</html>'''
)
