(function() {
  document.addEventListener('DOMContentLoaded', function() {
    // disable approval while WIP
    const approve_button = document.querySelector('button[value="approve"]');
    if (approve_button) {
      const issue_title = document.querySelector('#issue-title').textContent;
      if (issue_title.startsWith('WIP:')) {
        approve_button.disabled = true;
        approve_button.textContent = approve_button.textContent + " (still WIP)";
      }
    }

    // hide consecutive pushes
    const pull_comments = document.querySelectorAll('.pull .comment-list .timeline-item')
    var previous_forced_push = null;
    var last_visible_commit = null;
    for (var i=0; i<pull_comments.length; i++) {
      var is_this_forced_push = (pull_comments[i].textContent.search('a forcé') != -1 ||
                                 pull_comments[i].textContent.search('a soumit de force') != -1 ||
                                 pull_comments[i].textContent.search('force-pushed') != -1)
      if (is_this_forced_push) {
        if (previous_forced_push) {
          // hide previous
          previous_forced_push.style.display = 'none'
        }
        previous_forced_push = pull_comments[i]
      } else {
        if (previous_forced_push) {
          var compare_button = previous_forced_push.querySelector('.ui.compare.label');
          var path = null;
          var new_href = null;
          if (compare_button) {
            var previous_forced_push_commit = compare_button.href.split('/').at(-1).split('..')[1];
            if (last_visible_commit) {
                var path = compare_button.href.split('/').slice(0, -1).join('/') + '/';
                compare_button.href = path + last_visible_commit + '..' + previous_forced_push_commit;
            }
            last_visible_commit = previous_forced_push_commit;
          }
        }
        var commit_list = pull_comments[i].querySelectorAll('.ui.sha.label');
        if (commit_list.length) {
          last_visible_commit = Array.from(commit_list).at(-1).href.split('/').at(-1);
        }
        previous_forced_push = null
      }
    }

    if (isFreezePeriod()) {
      document.body.classList.add('pk-freeze-period')
    }

  });
}());

function isFreezePeriod(date = new Date()) {
  function deploydates() {
    const thursdays = []

    function thursdaysOnMonth(month, nb) {
      for(let i = 1; thursdays.length < nb; i++) {
        const day = new Date(date.getFullYear(), month, i);
        // console.log(day)
        if (day.getDay() === 4) thursdays.push(day)
      }
    }

    thursdaysOnMonth(date.getMonth(), 4);
    if (date.getTime() > thursdays.at(-1).getTime())
      thursdaysOnMonth(date.getMonth() + 1, 6)

    const deployThusdays = thursdays.filter( (el, index) => {
      return index % 2 === 1
    })
    return deployThusdays
  }

  const freezePeriods = deploydates().map( time => {
    return {
      start: new Date(time.getFullYear(), time.getMonth(), time.getDate() - 6, 6, 00, 00),
      end: new Date(time.getFullYear(), time.getMonth(), time.getDate() +1, 01, 00, 00)
    }
  })

  const isFreeze = () => {
    for(const freezeP of freezePeriods) {
      if (date.getTime() > freezeP.start.getTime() &&  date.getTime() < freezeP.end.getTime())
        return true;
    }
  }

  return isFreeze();
}
