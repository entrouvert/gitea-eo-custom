VERSION=`git describe | sed 's/^v//; s/-/./g'`
NAME = gitea-eo-custom
prefix = /usr

all:

install:
	mkdir -p $(DESTDIR)$(prefix)/lib/gitea-eo-custom/
	cp -r scripts $(DESTDIR)$(prefix)/lib/gitea-eo-custom/
	mkdir -p $(DESTDIR)$(prefix)/../var/lib/gitea/custom/
	cp -r public $(DESTDIR)$(prefix)/../var/lib/gitea/custom/
	cp -r templates $(DESTDIR)$(prefix)/../var/lib/gitea/custom/

DIST_FILES = \
	Makefile \
	LICENSE \
	README.md \
	scripts \
	public \
	templates

clean:
	rm -rf sdist

dist: clean
	-mkdir sdist
	rm -rf sdist/$(NAME)-$(VERSION)
	mkdir -p sdist/$(NAME)-$(VERSION)
	for i in $(DIST_FILES); do \
		cp -R "$$i" sdist/$(NAME)-$(VERSION); \
	done

dist-bzip2: dist
	-mkdir sdist
	cd sdist && tar cfj ../sdist/$(NAME)-$(VERSION).tar.bz2 $(NAME)-$(VERSION)

version:
	@(echo $(VERSION))

name:
	@(echo $(NAME))

fullname:
	@(echo $(NAME)-$(VERSION))
